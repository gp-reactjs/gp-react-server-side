import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { promise } from 'redux-promise';

import reducer from './reducers';
import Routes from './Routes';

const store = createStore(reducer, promise);

ReactDOM.render(
  <Provider store={store}>
    <Routes />
  </Provider>,
  document.querySelector("#root")
);
