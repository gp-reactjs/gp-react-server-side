import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

import Home from './components/Home';
import SignUp from './components/SignUp';
import SignIn from './components/SignIn';
import Posts from './components/Posts';
import AddPost from './components/AddPost';


class App extends Component {

  render() {
    return(
      <Router>
        <div>
          <nav className="navbar navbar-inverse">
            <div className="container-fluid">
              <div className="navbar-header">
                <a className="navbar-brand" href="#">WebSiteName</a>
              </div>
              <ul className="nav navbar-nav">
                <li><Link to="/">Home</Link></li>
                <li><Link to="/sign-in">Sing In</Link></li>
                <li><Link to="/sign-up">Sing Up</Link></li>
                <li><Link to="/posts">Posts</Link></li>
                {/* <li><Link to="/details/0107">Details</Link></li> */}
              </ul>
            </div>
          </nav>
          <hr/>

          <Route exact={true} exact path="/" component={Home}/>
          <Route exact={true} path="/sign-in" component={SignIn}/>
          <Route exact={true} path="/sign-up" component={SignUp}/>
          <Route exact={true} path="/posts" component={Posts}/>
          <Route exact={true} path="/posts/add-post" component={AddPost}/>
          {/* <Route path="/details/:detailsid" component={Details}/> */}
        </div>
      </Router>
    )
  }
}

export default App;
