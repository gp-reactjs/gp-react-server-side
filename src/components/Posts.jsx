import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPosts } from '../actions';
import {

  Link
} from 'react-router-dom';

class Posts extends Component {
  constructor(props) {
    super(props);
    const btnUrl = this.props.match.path
  }

  componentWillMount() {
    this.props.fetchPosts();
  }


  render() {
    return (
      <div className="col-sm-6 col-sm-offset-3">
        <form className="form-horizontal">
          <div className="form-group">
            <Link className="btn btn-primary pull-right" to={`${this.props.match.path}/add-post`}>Add New Post</Link>
          </div>
        </form>
        <hr />
        <ul className="list-group">
          <li className="list-group-item">Gu</li>
          <li className="list-group-item">Bu</li>
          <li className="list-group-item">Gugu</li>
          <li className="list-group-item">Bubu</li>
        </ul>
      </div>
    );
  }
}

// function mapStateToProps(state) {
//   return {
//     all_posts: state
//   }
// }

export default connect(null, { fetchPosts })(Posts);
