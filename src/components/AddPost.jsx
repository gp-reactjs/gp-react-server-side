import React, { Component } from 'react';
// import { reducer as formReducer } from 'redux-form'

class AddPost extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "",
      cat: "",
      content: ""
    }
  }

  savePost() {

  }

  render() {
    return (
      <div className="col-sm-4 col-sm-offset-4">
        <form className="form-horizontal">
          <div className="form-group">
            <label className="control-label col-sm-2">Title:</label>
            <div className="col-sm-10">
              <input type="text" className="form-control" onChange={event => this.setState({title: event.target.value})} id="title" placeholder="Enter title" />
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Category:</label>
            <div className="col-sm-10">
              <input type="text" className="form-control" onChange={event => this.setState({cat: event.target.value})} id="cat" placeholder="Enter password" />
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Content:</label>
            <div className="col-sm-10">
              <textarea className="form-control" onChange={event => this.setState({content: event.target.value})} id="content" placeholder="Enter message" ></textarea>
            </div>
          </div>
          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <button type="button" onClick={() => this.savePost()} className="btn btn-default">Sign Up</button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default AddPost;
