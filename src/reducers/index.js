import { FETCH_POSTS } from '../constants';

const INITIAL_STATE = { all: [], post: null }

const allposts = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case FETCH_POSTS:
      return { ...state, all: action.payload.data }
    default:
    return state;
  }

}

export default allposts;
